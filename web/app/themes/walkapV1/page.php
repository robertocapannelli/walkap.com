<?php get_header(); ?>
<div class="wrapper" id="container">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article id="post-<?php the_ID();?>" <?php post_class();?> itemscope>
			<div class="wrapper_article">
				<header>
					<h1 class="entry-title">
						<?php the_title(); ?>
					</h1>
				</header>		
				<div class="container" role="main"><?php the_content(); ?></div>
				<footer class="main_footer" role="contentinfo">
				</footer>
			</div>
		</article>
	<?php endwhile; else : ?>
		<article class="post">
        	<header>
				<h1>Not Found</h1>
        	</header>
			<p>Sorry, but you are looking for something that isn't here. Try again, maybe you'll be luckier next time. <br> Let's think BIG!</p>
		</article>
	<?php endif; ?>
</div>
<?php get_footer(); ?>
