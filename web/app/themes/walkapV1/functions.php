<?php
// Custom WP login
function my_login_stylesheet() { ?>
    <link rel="stylesheet" id="custom_wp_admin_css"  href="<?php echo get_bloginfo( 'stylesheet_directory' ) . '/wplogin.css'; ?>" type="text/css" media="all" />
<?php }

add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

// Redirect /?s to /search/ as http://txfx.net/wordpress-plugins/nice-search/
function roots_nice_search_redirect() {
	if (is_search() && strpos($_SERVER['REQUEST_URI'], '/wp-admin/') === false && strpos($_SERVER['REQUEST_URI'], '/search/') === false) {
		wp_redirect(home_url('/search/' . str_replace(array(' ', '%20'), array('+', '+'), urlencode(get_query_var( 's' )))), 301);
    exit();
	}
}
add_action('template_redirect', 'roots_nice_search_redirect');
function roots_search_query($escaped = true) {
	$query = apply_filters('roots_search_query', get_query_var('s'));
	if ($escaped) {
    	$query = esc_attr($query);
	}
 	return urldecode($query);
}
add_filter('get_search_query', 'roots_search_query');


// Remove WordPress version from RSS feed
function roots_no_generator() { return ''; }
add_filter('the_generator', 'roots_no_generator');


// Clean WP head
function roots_noindex() {
	if (get_option('blog_public') === '0') {
    	echo '<meta name="robots" content="noindex,nofollow">', "\n";
  	}
}
function roots_rel_canonical() {
	if (!is_singular()) {
		return;
  	}

	global $wp_the_query;
	if (!$id = $wp_the_query->get_queried_object_id()) {
		return;
  	}

	$link = get_permalink($id);
	echo "\t<link rel=\"canonical\" href=\"$link\">\n";
}
function roots_remove_recent_comments_style() {
	global $wp_widget_factory;
	if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
		remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
	}
}
function roots_gallery_style($css) {
	return preg_replace("/<style type='text\/css'>(.*?)<\/style>/s", '', $css);
}

function roots_head_cleanup() {
	// http://wpengineer.com/1438/wordpress-header/
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
	remove_action('wp_head', 'noindex', 1);
	add_action('wp_head', 'roots_noindex');
	remove_action('wp_head', 'rel_canonical');
	//add_action('wp_head', 'roots_rel_canonical');
	add_action('wp_head', 'roots_remove_recent_comments_style', 1);
	add_filter('gallery_style', 'roots_gallery_style');

	// stop Gravity Forms from outputting CSS since it's linked in header.php
	if (class_exists('RGForms')) {
		update_option('rg_gforms_disable_css', 1);
	}

	// deregister l10n.js (new since WordPress 3.1)
	// why you might want to keep it: http://wordpress.stackexchange.com/questions/5451/what-does-l10n-js-do-in-wordpress-3-1-and-how-do-i-remove-it/5484#5484
	// don't load jQuery through WordPress since it's linked in header.php
	if (!is_admin()) {
		wp_deregister_script('l10n');
	}
}
add_action('init', 'roots_head_cleanup');

/*
Start load theme CSS and scripts
*/
function load_theme_styles() { 
  //wp_register_style('normalize', get_template_directory_uri() . '/css/normalize.min.css', array(), '1.0.1', 'all');
  //wp_enqueue_style('normalize');
  wp_register_style('style', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
  wp_enqueue_style('style');
}
function load_theme_scripts() {
	wp_enqueue_script('modernizr', get_template_directory_uri() .'/js/vendor/modernizr-2.6.2.min.js', false, '2.6.2', false);
	wp_deregister_script('jquery');
	$url = 'http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js'; // the URL to check against
	$test_url = @fopen($url,'r'); // test parameters
	if ($test_url != null) {
		wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js', false, '1.8.3', true);
	} else {
		wp_register_script('jquery', get_template_directory_uri() .'/js/vendor/jquery-1.8.3.min.js', __FILE__, false, '1.8.3', false);
	}
    wp_enqueue_script('jquery');	
	wp_enqueue_script('main', get_template_directory_uri() .'/js/main.js', array('jquery'), '1.0', true);
	wp_enqueue_script('plugins', get_template_directory_uri() .'/js/plugins.js', array('jquery'), '1.0', true);
	
}
add_action('wp_enqueue_scripts', 'load_theme_styles');
add_action('wp_enqueue_scripts', 'load_theme_scripts');
/*
End load theme CSS and scripts
*/

// Cleanup gallery_shortcode()
function roots_gallery_shortcode($attr) {
	global $post, $wp_locale;

	static $instance = 0;
	$instance++;

	// Allow plugins/themes to override the default gallery template.
	$output = apply_filters('post_gallery', '', $attr);
	if ( $output != '' )
		return $output;

	// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
	if ( isset( $attr['orderby'] ) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( !$attr['orderby'] )
			unset( $attr['orderby'] );
	}

	extract(shortcode_atts(array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post->ID,
		'icontag'    => 'figure',
		'captiontag' => 'figcaption',
		'columns'    => 3,
		'size'       => 'thumbnail',
		'include'    => '',
		'exclude'    => ''
	), $attr));

	$id = intval($id);
	if ( 'RAND' == $order )
		$orderby = 'none';

	if ( !empty($include) ) {
		$include = preg_replace( '/[^0-9,]+/', '', $include );
		$_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} elseif ( !empty($exclude) ) {
		$exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
		$attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	} else {
		$attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	}

	if ( empty($attachments) )
		return '';

	if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment )
			$output .= wp_get_attachment_link($att_id, $size, true) . "\n";
		return $output;
	}

	$captiontag = tag_escape($captiontag);
	$columns = intval($columns);
	$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
	$float = is_rtl() ? 'right' : 'left';

	$selector = "gallery-{$instance}";

	$gallery_style = $gallery_div = '';
	if ( apply_filters( 'use_default_gallery_style', true ) )
		$gallery_style = "";
	$size_class = sanitize_html_class( $size );
	$gallery_div = "<section id='$selector' class='clearfix gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";
	$output = apply_filters( 'gallery_style', $gallery_style . "\n\t\t" . $gallery_div );

	$i = 0;
	foreach ( $attachments as $id => $attachment ) {
		// make the gallery link to the file by default instead of the attachment
		// thanks to Matt Price (countingrows.com)
    $link = isset($attr['link']) && $attr['link'] === 'attachment' ? 
      wp_get_attachment_link($id, $size, true, false) : 
      wp_get_attachment_link($id, $size, false, false);
		$output .= "
			<{$icontag} class=\"gallery-item\">
				$link
			";
		if ( $captiontag && trim($attachment->post_excerpt) ) {
			$output .= "
				<{$captiontag} class=\"gallery-caption\">
				" . wptexturize($attachment->post_excerpt) . "
				</{$captiontag}>";
		}
		$output .= "</{$icontag}>";
		if ( $columns > 0 && ++$i % $columns == 0 )
			$output .= '';
	}

	$output .= "</section>\n";

	return $output;
}
remove_shortcode('gallery');
add_shortcode('gallery', 'roots_gallery_shortcode');


// Set the post revisions to 5 unless the constant was set in wp-config.php to avoid DB bloat
if (!defined('WP_POST_REVISIONS')) define('WP_POST_REVISIONS', 5);


// Allow more tags in TinyMCE including iframes
function roots_change_mce_options($options) {
	$ext = 'pre[id|name|class|style],iframe[align|longdesc|name|width|height|frameborder|scrolling|marginheight|marginwidth|src]';	
	if (isset($initArray['extended_valid_elements'])) {
		$options['extended_valid_elements'] .= ',' . $ext;
	} else {
		$options['extended_valid_elements'] = $ext;
	}
	return $options;
}

add_filter('tiny_mce_before_init', 'roots_change_mce_options');

// Menu dinamici WP3
function register_my_menus() {
register_nav_menus(array('header-menu' => __( 'Header Menu' )));
register_nav_menus(array('sidebar-menu' => __( 'Sidebar Menu' )));
}
add_action( 'init', 'register_my_menus' );

// Wrapper function around wp_nav_menu() that will cache the wp_nav_menu for all tag/category pages used in the nav menus - see http://lookup.hitchhackerguide.com/wp_nav_menu for $args 
function hh_cached_nav_menu( $args = array(), $prime_cache = false ) {
	global $wp_query;

	$queried_object_id = empty( $wp_query->queried_object_id ) ? 0 : (int) $wp_query->queried_object_id;

	// If design of navigation menus differs per queried object use the key below
	// $nav_menu_key = md5( serialize( $args ) . '-' . $queried_object_id );

	// Otherwise
	$nav_menu_key = md5( serialize( $args ) );

	$my_args = wp_parse_args( $args );
	$my_args = apply_filters( 'wp_nav_menu_args', $my_args );
	$my_args = (object) $my_args;

	if ( ( isset( $my_args->echo ) && true === $my_args->echo ) || !isset( $my_args->echo ) ) {
		$echo = true;
	} else {
		$echo = false;
	}

	$skip_cache = false;
	$use_cache = ( true === $prime_cache ) ? false : true;

	// If design of navigation menus differs per queried object comment out this section
	//*
	if ( is_singular() ) {
		$skip_cache = true;
	} else if ( !in_array( $queried_object_id, hh_get_nav_menu_cache_objects( $use_cache ) ) ) {
		$skip_cache = true;
	}
	//*/

	if ( true === $skip_cache || true === $prime_cache || false === ( $nav_menu = get_transient( $nav_menu_key ) ) ) {
		if ( false === $echo ) {
			$nav_menu = wp_nav_menu( $args );
		} else {
			ob_start();
			wp_nav_menu( $args );
			$nav_menu = ob_get_clean();
		}
		if ( false === $skip_cache )
			set_transient( $nav_menu_key, $nav_menu );
	} 
	if ( true === $echo )
		echo $nav_menu;
	else
		return $nav_menu;
}

/**
 * Invalidate navigation menu when an update occurs
 */
function hh_update_nav_menu_objects( $menu_id = null, $menu_data = null ) {
	hh_cached_nav_menu( array( 'echo' => false ), $prime_cache = true );
}
add_action( 'wp_update_nav_menu', 'hh_update_nav_menu_objects' );

/** 
 * Helper function that returns the object_ids we'd like to cache
 */
function hh_get_nav_menu_cache_objects( $use_cache = true ) {
	$object_ids = get_transient( 'hh_nav_menu_cache_object_ids' );
	if ( true === $use_cache && !empty( $object_ids ) ) {
		return $object_ids;
	}

	$object_ids = $objects = array();

	$menus = wp_get_nav_menus();
	foreach ( $menus as $menu_maybe ) {
		if ( $menu_items = wp_get_nav_menu_items( $menu_maybe->term_id ) ) {
			foreach( $menu_items as $menu_item ) {
				if ( preg_match( "#.*/category/([^/]+)/?$#", $menu_item->url, $match ) )
					$objects['category'][] = $match[1];
				if ( preg_match( "#.*/tag/([^/]+)/?$#", $menu_item->url, $match ) )
					$objects['post_tag'][] = $match[1];
			}
		}
	}
	if ( !empty( $objects ) ) {
		foreach( $objects as $taxonomy => $term_names ) {
			foreach( $term_names as $term_name ) {
				$term = get_term_by( 'slug', $term_name, $taxonomy );
				if ( $term )
					$object_ids[] = $term->term_id;
			}
		}
	}

	$object_ids[] = 0; // that's for the homepage

	set_transient( 'hh_nav_menu_cache_object_ids', $object_ids );
	return $object_ids;
}

// Recupero prima immagine del post
function get_first_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];

  if(empty($first_img)){ //Defines a default image - enter manually the image path
    $first_img = (get_bloginfo('template_url') . "/img/segnaposto.gif");
    
  }
  return $first_img;
}

// Custom Fields Template
if ( !class_exists('myCustomFields') ) {

	class myCustomFields {
		/**
		* @var  string  $prefix  The prefix for storing custom fields in the postmeta table
		*/
		var $prefix = '';
		/**
		* @var  array  $postTypes  An array of public custom post types, plus the standard "post" and "page" - add the custom types you want to include here
		*/
		var $postTypes = array( "post", "customposttype" );
		/**
		* @var  array  $customFields  Defines the custom fields available
		*/
		var $customFields =	array(
			
			array(
				"name"			=> "Fonte",
				"title"			=> "Fonte",
				"description"	=> "Enter the source of the content",
				"type"			=>	"text",
				"scope"			=>	array( "post" ),
				"capability"	=> "edit_post"
			),
			array(
				"name"    		=> "designer",
				"title"			=> "Designer",
				"description"	=> "Enter the name of the designer who made this work (UI designer, UX desinger...)",
				"type"			=> "text",
				"scope"			=>  array("post"),
				"capability"	=>  "edit_post"
			
			),
			array(
				"name"			=> "developer",
				"title"			=> "Sviluppatori",
				"description"	=> "Enter the name of the developer who made this work (HTML5, CSS3, php, javascript...)",
				"type"			=> "text",
				"scope"			=> array("post"),
				"capability"	=> "edit_post"
				
			),
			array(
				"name"			=> "contributi",
				"title"			=> "Contributi",
				"description"	=> "Enter the names of the contributors who help you to made this work (photographer, copywriter, SEO specialist, video maker...)",
				"type"			=> "text",
				"scope"			=> array("post"),
				"capability"	=> "edit_post"
			),
			array(
				"name"			=> "tecnologie",
				"title"			=> "Tecnologie",
				"description"	=> "Enter the type of the technologies used for this work(HTML5, CSS3, php, javascript, asp, java...)",
				"type"			=> "text",
				"scope"			=> array("post"),
				"capability"	=> "edit_post"
			),
			array(
				"name"			=> "preview",
				"title"			=> "Live preview",
				"description"	=> "Enter the link to see the online work",
				"type"			=> "text",
				"scope"			=> array("post"),
				"capability"	=> "edit_post"
			),
			array(
				"name"			=> "agenzia",
				"title"			=> "Agenzia",
				"description"	=> "Enter the agency name",
				"type"			=> "text",
				"scope"			=> array("post"),
				"capability"	=> "edit_post"
			),
			array(
				"name"			=> "account",
				"title"			=> "Account",
				"description"	=> "Enter the account's name",
				"type"			=> "text",
				"scope"			=> array("post"),
				"capability"	=> "edit_post"
			),
			
		);
		/**
		* PHP 4 Compatible Constructor
		*/
		function myCustomFields() { $this->__construct(); }
		/**
		* PHP 5 Constructor
		*/
		function __construct() {
			add_action( 'admin_menu', array( &$this, 'createCustomFields' ) );
			add_action( 'save_post', array( &$this, 'saveCustomFields' ), 1, 2 );
			// Comment this line out if you want to keep default custom fields meta box
			add_action( 'do_meta_boxes', array( &$this, 'removeDefaultCustomFields' ), 10, 3 );
		}
		/**
		* Remove the default Custom Fields meta box
		*/
		function removeDefaultCustomFields( $type, $context, $post ) {
			foreach ( array( 'normal', 'advanced', 'side' ) as $context ) {
				foreach ( $this->postTypes as $postType ) {
					remove_meta_box( 'postcustom', $postType, $context );
				}
			}
		}
		/**
		* Create the new Custom Fields meta box
		*/
		function createCustomFields() {
			if ( function_exists( 'add_meta_box' ) ) {
				foreach ( $this->postTypes as $postType ) {
					add_meta_box( 'my-custom-fields', 'Opzioni del tema', array( &$this, 'displayCustomFields' ), $postType, 'normal', 'high' );
				}
			}
		}
		/**
		* Display the new Custom Fields meta box
		*/
		function displayCustomFields() {
			global $post;
			?>

			<div class="form-wrap">
				<?php
				wp_nonce_field( 'my-custom-fields', 'my-custom-fields_wpnonce', false, true );
				foreach ( $this->customFields as $customField ) {
					// Check scope
					$scope = $customField[ 'scope' ];
					$output = false;
					foreach ( $scope as $scopeItem ) {
						switch ( $scopeItem ) {
							default: {
								if ( $post->post_type == $scopeItem )
									$output = true;
								break;
							}
						}
						if ( $output ) break;
					}
					// Check capability
					if ( !current_user_can( $customField['capability'], $post->ID ) )
						$output = false;
					// Output if allowed
					if ( $output ) { ?>
						<div class="form-field form-required">
							<?php
							switch ( $customField[ 'type' ] ) {
							
								case "dropdown": {
									// Dropdown
									echo '<label for="' . $this->prefix . $customField[ 'name' ] .'" style="display:inline;"><b>' . $customField[ 'title' ] . '</b></label>&nbsp;&nbsp;';
									echo '<select name="'. $this->prefix . $customField['name'] .'" id="' . $this->prefix . $customField[ 'name' ] . '">';
									foreach ($customField['options'] as $key => $value){
										$selected = '';
										if (get_post_meta( $post->ID, $this->prefix . $customField['name'], true ) == $value) {
											$selected = ' selected="selected" ';
										}
										echo '<option value="'. $value .'" '.$selected.'>'. $value .'</option>';
									}
									echo '</select>';
									break;
								}
								case "checkbox": {
									// Checkbox
									echo '<label for="' . $this->prefix . $customField[ 'name' ] .'" style="display:inline;"><b>' . $customField[ 'title' ] . '</b></label>&nbsp;&nbsp;';
									echo '<input type="checkbox" name="' . $this->prefix . $customField['name'] . '" id="' . $this->prefix . $customField['name'] . '" value="yes"';
									if ( get_post_meta( $post->ID, $this->prefix . $customField['name'], true ) == "yes" )
										echo ' checked="checked"';
									echo '" style="width: auto;" />';
									break;
								}
								case "textarea":
								case "wysiwyg": {
									// Text area
									echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label>';
									echo '<textarea name="' . $this->prefix . $customField[ 'name' ] . '" id="' . $this->prefix . $customField[ 'name' ] . '" columns="30" rows="3">' . htmlspecialchars( get_post_meta( $post->ID, $this->prefix . $customField[ 'name' ], true ) ) . '</textarea>';
									// WYSIWYG
									if ( $customField[ 'type' ] == "wysiwyg" ) { ?>
										<script type="text/javascript">
											jQuery( document ).ready( function() {
												jQuery( "<?php echo $this->prefix . $customField[ 'name' ]; ?>" ).addClass( "mceEditor" );
												if ( typeof( tinyMCE ) == "object" && typeof( tinyMCE.execCommand ) == "function" ) {
													tinyMCE.execCommand( "mceAddControl", false, "<?php echo $this->prefix . $customField[ 'name' ]; ?>" );
												}
											});
										</script>
									<?php }
									break;
								}
								default: {
									// Plain text field
									echo '<label for="' . $this->prefix . $customField[ 'name' ] .'"><b>' . $customField[ 'title' ] . '</b></label>';
									echo '<input type="text" name="' . $this->prefix . $customField[ 'name' ] . '" id="' . $this->prefix . $customField[ 'name' ] . '" value="' . htmlspecialchars( get_post_meta( $post->ID, $this->prefix . $customField[ 'name' ], true ) ) . '" />';
									break;
								}
							}
							?>
							<?php if ( $customField[ 'description' ] ) echo '<p>' . $customField[ 'description' ] . '</p>'; ?>
						</div>
					<?php
					}
				} ?>
			</div>
			<?php
		}
		/**
		* Save the new Custom Fields values
		*/
		function saveCustomFields( $post_id, $post ) {
			if ( !isset( $_POST[ 'my-custom-fields_wpnonce' ] ) || !wp_verify_nonce( $_POST[ 'my-custom-fields_wpnonce' ], 'my-custom-fields' ) )
				return;
			if ( !current_user_can( 'edit_post', $post_id ) )
				return;
			if ( ! in_array( $post->post_type, $this->postTypes ) )
				return;
			foreach ( $this->customFields as $customField ) {
				if ( current_user_can( $customField['capability'], $post_id ) ) {
					if ( isset( $_POST[ $this->prefix . $customField['name'] ] ) && trim( $_POST[ $this->prefix . $customField['name'] ] ) ) {
						$value = $_POST[ $this->prefix . $customField['name'] ];
						// Auto-paragraphs for any WYSIWYG
						if ( $customField['type'] == "wysiwyg" ) $value = wpautop( $value );
						update_post_meta( $post_id, $this->prefix . $customField[ 'name' ], $value );
					} else {
						delete_post_meta( $post_id, $this->prefix . $customField[ 'name' ] );
					}
				}
			}
		}

	} // End Class

} // End if class exists statement

// instantiate the class
if ( class_exists('myCustomFields') ) {
	$myCustomFields_var = new myCustomFields();
}

// Special functions activation
function roots_setup() {
	// tell the TinyMCE editor to use editor-style.css
	// if you have issues with getting the editor to show your changes then use the following line:
	// add_editor_style('editor-style.css?' . time());
	add_editor_style('editor-style.css');
	
	// http://codex.wordpress.org/Post_Thumbnails
	add_theme_support('post-thumbnails');
	// set_post_thumbnail_size(150, 150, false);
	set_post_thumbnail_size(270, 300, false);
	// http://codex.wordpress.org/Post_Formats
	// add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));
}
add_action('after_setup_theme', 'roots_setup');

function data_type( $id = 0, $taxonomy, $before = '', $sep = '', $after = '' ) {
	$terms = get_the_terms( $id, $taxonomy );
	
	if ( is_wp_error( $terms ) )
		return $terms;
	
	if ( empty( $terms ) )
		return false;
	
	foreach ( $terms as $term ) {
		$link = get_term_link( $term, $taxonomy );
	    if ( is_wp_error( $link ) )
	    	return $link;
		$term_links[] = $term->name;
	}
	
	$term_links = apply_filters( "term_links-$taxonomy", $term_links );
	
	return $before . join( $sep, $term_links ) . $after;
}

// search filter excludine some categories
function mySearchFilter($query) {
    if ($query->is_search) {
        $excludeId = 460;
        $query->set('post__not_in', array(5,7));
    }
    return $query;
}
add_filter('pre_get_posts','mySearchFilter');


function search_url_rewrite_rule() {
	if ( is_search() && !empty($_GET['s'])) {
		wp_redirect(home_url("/search/") . urlencode(get_query_var('s')));
		exit();
	}	
}
add_action('template_redirect', 'search_url_rewrite_rule');

?>