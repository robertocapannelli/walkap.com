<!-- menu -->
	 <aside class="sidebar" role="complementary">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('leftsidebar') ) : ?>
					<div>
				<!-- google ad sense -->
				<script type="text/javascript"><!--
					google_ad_client = "ca-pub-5271751050627846";
					/* walkap */
					google_ad_slot = "0101308107";
					google_ad_width = 272;
					google_ad_height = 250;
				//-->
				</script><script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
			</div>
			
			<div class="philip-wood-ad">
				
				<a href="http://www.philip-wood.com">
					
					<img src="<?php bloginfo('template_url'); ?>/img/philip-wood-ad.jpg" alt="philip-wood-ad" width="250" height="250" />
					
				</a>
				
			</div>
			
			<section class="meta">
				<ul class="post-meta">
					<?php if (get_post_meta($post->ID, 'designer', true)) { ?>
					<li>
						<h3 itemprop="jobTitle">Designers:</h3> <span itemprop="author"><?php echo get_post_meta($post->ID, 'designer', true); ?></span>
					</li>
					<?php } ?>
					<?php if (get_post_meta($post->ID, 'developer', true)) { ?>
					<li>
						<h3 itemprop="jobTitle">Developers:</h3> <span itemprop="author"><?php echo get_post_meta($post->ID, 'developer', true); ?></span>
					</li>
					<?php } ?>
					<?php if (get_post_meta($post->ID, 'contributi', true)) { ?>
					<li>
						<h3>Contributi:</h3> <span itemprop="contributor"><?php echo get_post_meta($post->ID, 'contributi', true); ?></span>
					</li>
					<?php } ?>
					<?php if (get_post_meta($post->ID, 'Tecnologie', true)) { ?>
					<li>
						<h3>Tecnologie:</h3> <span><?php echo get_post_meta($post->ID, 'Tecnologie', true); ?></span>
					</li>
					<?php } ?>
					<?php if (get_post_meta($post->ID, 'preview', true)) { ?>
					<li>
						<h3>Live preview:</h3> <span><a href="<?php echo get_post_meta($post->ID, 'preview', true); ?>" target="_blank"><?php echo get_post_meta($post->ID, 'preview', true); ?></a></span>
					</li>
					<?php } ?>
					<?php if (get_post_meta($post->ID, 'account', true)) { ?>
					<li>
						<h3 itemprop="jobTitle">Account:</h3> <span><?php echo get_post_meta($post->ID, 'account', true); ?></span>
					</li>
					<?php } ?>
					<?php if (get_post_meta($post->ID, 'agenzia', true)) { ?>
					<li>
						<h3>Agenzia:</h3> <span><?php echo get_post_meta($post->ID, 'agenzia', true); ?></span>
					</li>
					<?php } ?>
				</ul>
			</section>
			<section class="search-section">
				<h4><?php _e('Search'); ?></h4>
				<form id="searchform" method="get" action="<?php bloginfo('home'); ?>">
					<input type="text" name="s" id="s" value="" placeholder="Search something..." />
				</form>
            </section>
			<?php if ( function_exists('wp_tag_cloud') ) : ?>
            <section class="tag-cloud-section">
				<!-- available in wp 2.3 -->
				<h4>Popular Tags</h4>
				<p>
					<?php wp_tag_cloud('smallest=10&largest=16&number=10'); ?>
				</p>
            </section>
			<?php endif; ?>
		<?php endif; ?>
    		<section class="related_post">
				<h4>Related posts</h4>
				<ul>
    		      <?php $orig_post = $post;  
    					global $post;  
    					$tags = wp_get_post_tags($post->ID);  
    					if ($tags) {  
    						$tag_ids = array();  
    						foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;  
    						$args=array(  
    						'tag__in' => $tag_ids,  
    						'post__not_in' => array($post->ID),  
    						'posts_per_page'=>5, // Number of related posts to display.  
    						'caller_get_posts'=>1  
    						);  
    		  
    						$my_query = new wp_query( $args );   
    						while( $my_query->have_posts() ) {  
    							$my_query->the_post(); ?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a><br>
						<span itemprop="datePublished"><?php the_time('l j F Y'); ?></span>
					</li>
    		        
    		        <? }  
    				}  
    				$post = $orig_post;  
    				wp_reset_query();  
    				?>  
				</ul>
			</section>
		</aside>
<!-- end menu -->