<!-- comments -->
<?php // Do not delete these lines
	if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if (!empty($post->post_password)) { // if there's a password
		if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie
			?>

			<p class="nocomments">This post is password protected. Enter the password to view comments.</p>

			<?php
			return;
		}
	}

	/* This variable is for alternating comment background */
	$oddcomment = 'alt';


	if ($comments) : ?>
<section id="comments" class="item entry clear">
	<h2><?php comments_number('No Responses', 'One Response', '% Responses' );?> to &#8220;<?php the_title(); ?>&#8221;</h2>

		<?php foreach ($comments as $comment) : ?>
		<article class="<?php if (1 == $comment->user_id) $oddcomment = "comment-color-author"; echo $oddcomment; ?> article_comment" id="comment-<?php comment_ID() ?>">
			<?php if(function_exists('get_avatar')) { echo get_avatar($comment, '80'); } ?>
			<address class="comment-author"><?php comment_author_link() ?></address>
			<?php if ($comment->comment_approved == '0') : ?>
			<p>Your comment is awaiting moderation.</p>
			<?php endif; ?>
			<?php comment_text() ?>
			<footer class="commentmetadata"><a href="#comment-<?php comment_ID() ?>"><?php comment_date('F jS, Y') ?> - <?php comment_time() ?></a> <?php edit_comment_link('e','',''); ?></footer>
		</article>

	<?php /* Changes every other comment to a different class */
		if ('alt' == $oddcomment) $oddcomment = '';
		else $oddcomment = 'alt';
	?>

	<?php endforeach; /* end for each comment */ ?>
</section>

 <?php else : // this is displayed if there are no comments so far ?>

	<?php if ('open' == $post->comment_status) : ?>
		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
<section id="comments" class="item entry clear">
		<p class="nocomments">Comments are closed.</p>
</section>

	<?php endif; ?>
<?php endif; ?>


<?php if ('open' == $post->comment_status) : ?>
<section id="respond" class="item entry comments clear">
	<!--<h2>Leave a comment</h2>-->
	
	<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
	<p>You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php the_permalink(); ?>">logged in</a> to post a comment.</p></section>
	<?php else : ?>
	<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
		<?php if ( $user_ID ) : ?>
		<p>Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="Log out of this account">Logout &raquo;</a></p>
		<?php else : ?>
		<label for="author">Name <?php if ($req) echo "(required)"; ?></label>
		<input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1" placeholder="Insert your name" />
		<label for="email">Mail (will not be published) <?php if ($req) echo "(required)"; ?></label>
		<input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2" placeholder="Insert your e-mail" />
		<label for="url">Website</label>
		<input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="22" tabindex="3" placeholder="Insert your website" />
		<?php endif; ?>
		<!--<p><small><strong>XHTML:</strong> You can use these tags: <?php echo allowed_tags(); ?></small></p>-->
		<textarea name="comment" id="comment" style="width: 95%" rows="10" tabindex="4" placeholder="Write a comment"></textarea>
		<input name="submit" type="submit" id="submit" tabindex="5" value="Submit Comment" />
		<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
		<?php do_action('comment_form', $post->ID); ?>
	</form>
<?php endif; // If registration required and not logged in ?>
</section>
<?php endif; // if you delete this the sky will fall on your head ?>

<!-- end comments -->
