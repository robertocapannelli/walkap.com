<?php get_header(); ?>
<div class="wrapper" id="container">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article id="post-<?php the_ID();?>" <?php post_class();?> itemscope>
			<div class="wrapper_article">
				<header>
					<h1 class="entry-title">
						<?php the_title(); ?>						
					</h1>
					<div class="meta">
                    	<div class="tags">
				 			<?php the_tags('<img src="http://www.walkap.com/wp-content/themes/walkap_theme/img/tag.png" alt="Tags"/> ', ', ', ' '); ?>
                        </div>
                    	<time datetime="<?php the_time('c'); ?>" class="date updated" itemprop="datePublished"><?php the_time('l j F Y') ?></time>
						<div class="vcard author visuallyhidden">
							<span class="fn" itemprop="author"><?php the_author(); ?></span>
						</div>
					</div>
				</header>		
            	<div class="container" role="main">
			 		<?php the_content(); ?>
			 		<?php $attachments = get_posts(array( 'post_type' => 'attachment', 'post_parent' => $post->ID)); 
				 		if($attachments){ 
				 		foreach($attachments as $attachment){ ?>
			 		<figure>
			 			<a href="<?php echo get_post_meta($post->ID, 'preview', true); ?>" target="_blank">
				 			<?php
					 			$id_allegato = $attachment->ID;
					 			echo wp_get_attachment_image($id_allegato, 'large'); ?>
			 			</a>
			 		</figure>
			 		
			 		<?php 		}
			 				}			
			 		 ?>
			 		<?php comments_template(); ?>
			 		
				</div>
				<?php get_sidebar(); ?>
    			<footer class="main_footer">
					<?php the_category();?>
                	<div class="share">
                   		
                   		<a href="https://twitter.com/share" class="twitter-share-button" data-via="Rcapannelli" data-count="none" data-hashtags="walkap">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                  		
                  		<div class="fb-like" data-href="<?php echo the_permalink(); ?>" data-send="false" data-layout="button_count" data-width="400" data-show-faces="false" data-font="verdana"></div>
                  		
                  		<!-- Inserisci questo tag nel punto in cui vuoi che sia visualizzato l'elemento pulsante +1. -->
                  		<div class="g-plusone" data-size="tall" data-annotation="none"></div>

                  		<!-- Inserisci questo tag dopo l'ultimo tag di pulsante +1. -->
                  		<script type="text/javascript">
                  		  window.___gcfg = {lang: 'it'};
                  		
                  		  (function() {
                  		    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                  		    po.src = 'https://apis.google.com/js/plusone.js';
                  		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                  		  })();
                  		</script>
                  		
                	</div>
				</footer>
			</div>
		</article>
	<?php endwhile; else : ?>
		<article class="post">
        	<header>
				<h1>Not Found</h1>
        	</header>
			<p>Sorry, but you are looking for something that isn't here. Try again, maybe you'll be luckier next time. <br> Let's think BIG!</p>
		</article>
	<?php endif; ?>
</div>
<?php get_footer(); ?>