$(window).load(function() {
var $container = $('.blog #container, .archive #container, .search #container');
//$container.imagesLoaded(function(){
$container.masonry({
itemSelector : '.blog .type-post, .archive .type-post, .search .type-post' 
});
$('.blog #container, .archive #container, .search #container').masonry({
// options...
isAnimated: true,
isResizable: true,
isFitWidth: true,
isRTL: false
});
//});
//INFINITE SCROLLING
$container.infinitescroll({
navSelector  : '#page-nav',    // selector for the paged navigation 
nextSelector : '#page-nav a',  // selector for the NEXT link (to page 2)
itemSelector : '.post',     // selector for all items you'll retrieve
loading: {
finishedMsg: 'End',
//img: 'http://i.imgur.com/6RMhx.gif'
},
localMode	: true},
// trigger Masonry as a callback
function( newElements ) {
// hide new items while they are loading
var $newElems = $( newElements ).css({ opacity: 0 });
// ensure that images load before adding to masonry layout
$newElems.imagesLoaded(function(){
// show elems now they're ready
$newElems.animate({ opacity: 1 });
$container.masonry( 'appended', $newElems, true ); 
});
});

//links rolling
var supports3DTransforms =  document.body.style['webkitPerspective'] !== undefined || 
                            document.body.style['MozPerspective'] !== undefined;
function linkify( selector ) {
    if( supports3DTransforms ) {
        var nodes = document.querySelectorAll( selector );
        for( var i = 0, len = nodes.length; i < len; i++ ) {
            var node = nodes[i];
            if( !node.className || !node.className.match( /roll/g ) ) {
                node.className += ' roll';
                node.innerHTML = '<span data-title="'+ node.text +'">' + node.innerHTML + '</span>';
            }
        };
    }
}

linkify( 'a' );
}); //closing for .load

$(document).ready(function(){
//JPANELMENU
var jPM = $.jPanelMenu({
menu: '#side-nav',
trigger: '.toggle-menu',
duration: 500,
keyboardShortcuts: false
});
jPM.on();
//NICESCROLL
	{ 
    $("html").niceScroll();
	}

//BACK TO TOP
// hide #back-top first
$("#back-top").hide();
// fade in #back-top
$(function () {
$(window).scroll(function () {
if ($(this).scrollTop() > 90) {
$('#back-top').slideDown();
} else {
$('#back-top').slideUp();
}
});
// scroll body to 0px on click
$('#back-top a').click(function () {
$('body,html').animate({
scrollTop: 0
}, 2000);
return false;
});
});
// Check if browser supports HTML5 input placeholder
function supports_input_placeholder() {
var i = document.createElement('input');
return 'placeholder' in i;
}
// Change input text on focus
if (!supports_input_placeholder()) {
$(':text').focus(function(){
var self = $(this);
if (self.val() == self.attr('placeholder')) self.val('');
}).blur(function(){
var self = $(this), value = $.trim(self.val());
if(val == '') self.val(self.attr('placeholder'));
});
} else {
$(':text').focus(function(){
$(this).css('color', '#000');
});
}


}); //closing for .ready





