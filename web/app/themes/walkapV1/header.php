<!DOCTYPE html>
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html <?php language_attributes(); ?> class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->
<head>	
	<title><?php wp_title(''); ?></title>	
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="author" content="Roberto Capannelli">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico">
	<link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/apple-touch-icon.png">
	<link rel="author" href="<?php bloginfo('template_url'); ?>/humans.txt" type="text/plain" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
   	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_head(); ?>
</head>
<body  <?php body_class(); ?>>
<?php if (is_single()) {?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1&appId=266535806752338";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php } ?>
<!--[if lt IE 6]><p class=chromeframe><?php _e("Your browser is too <em>old!</em> <a href='http://browsehappy.com/'>Update your browser</a> or <a href='http://www.google.com/chromeframe/?redirect=true'>install Google Chrome Frame</a> to display correctly this web site.") ?></p><![endif]-->
	<nav class="side-nav" id="side-nav" >
    	<ul class="categories">
    		<?php wp_list_categories('title_li='); ?>
    		<li>
				<a href="https://forrst.com/people/robertocapannelli" target="_blank">forrst</a>
			</li>	
        </ul>
        <form id="panelsearch" method="get" action="<?php bloginfo('home');?>" role="search">
            <input type="text" name="s" id="p" placeholder="<?php _e('Search something...');?>" />
        </form>
        <ul class="social">
        	<li class="fb">
        		<a href="https://www.facebook.com/roberto.capannelli" target="_blank"></a>
        	</li>
            <li class="tw">
            	<a href="https://twitter.com/Rcapannelli" target="_blank"></a>
            </li>
            <li class="plus">
            	<a href="https://plus.google.com/u/0/103940657921818965810" target="_blank"></a>
            </li>
            <li class="feed">
            	<a href="http://www.walkap.com/feed/" target="_blank"></a>
            </li>
        </ul> 
        <p>walkap &copy;</p>
	</nav>
<header class="main_header">
	<a class="toggle-menu">
      &#9776;
    </a>
	<?php wp_nav_menu( array( 'container' => 'nav') ); ?>
	<p itemscope itemtype="http://schema.org/Person">
    			"When love and skills work together, expect a masterpiece".<br>
 				I’m <span itemprop="name">Roberto Capannelli</span> a <span itemprop="jobTitle">web
 				designer</span> based in <span itemprop="workLocation">Rome</span>, passionate for web technologies,
				music and everything around design. Let's Think BIG!
 	</p>
</header>