<?php get_header(); ?>
<div class="wrapper" id="container" role="main">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article id="post-<?php the_ID();?>" <?php post_class();?> itemscope itemtype="http://schema.org/Article">
			<div class="wrapper_article">
				<header>
					<h1>
						<a href="<?php the_permalink();?>" title="<?php the_title();?>" <?php if (!in_category(array(14, 4, 9, 11, 99))){ ?> target="_blank"<?php;}?> class="entry-title" rel="bookmark">
							<span itemprop="name"><?php the_title() ?></span>
						</a>
					</h1>
					<div class="meta">
						<time itemprop="datePublished" datetime="<?php the_time('c'); ?>" class="date updated entry-date" itemprop="datePublished"><?php the_time('l j F Y') ?></time>
						<div class="vcard author visuallyhidden" itemprop="author" itemscope itemtype="http://schema.org/Person">
							<span class="fn" itemprop="name"><?php the_author(); ?></span>
						</div>
               			<div class="tags">
				 			<?php the_tags('<img src="http://www.walkap.com/wp-content/themes/walkap_theme/img/tag.png" alt="Tags"/> ', ', ', ' '); ?>
                        </div>
					</div>
				</header>
				<?php if (has_post_thumbnail()){ ?>
					<figure>
                    	<a href="<?php echo get_permalink(); ?>" itemprop="url">
                    		<?php echo get_the_post_thumbnail($post_id,'thumbnail','itemprop=image');?>
                    	</a>
                    </figure>
                    <?php the_excerpt(); 
						  }elseif (in_category(array(6, 7, 12, 26))) { ?>
							<figure>
                        		<a href="<?php echo get_permalink(); ?>" target="_blank" itemprop="url">
                        			<span class="opacity"></span>
                            		<img src="<?php echo get_first_image(); ?>" alt="<?php echo get_the_title(); ?>" itemprop="image"/>
                            	</a>
                       	 	</figure>
							<?php the_excerpt(); 
						}elseif (in_category(11)){
							the_content();
						}else{ 
							the_excerpt(); 
						} ?>
				<footer class="main_footer" role="contentinfo">
					<?php the_category();?>
           			<span class="shadow"></span>
     			</footer>
			</div>
		</article>
	<?php endwhile; else : ?>
		<article class="post">
        	<header>
				<h1>Not Found</h1>
        	</header>
			<p>Sorry, but you are looking for something that isn't here. Try again, maybe you'll be luckier next time. <br> Let's think BIG!</p>
			<footer>
				<span class="shadow"></span>
			</footer>
		</article>
	<?php endif; ?>
</div>
<nav id="page-nav">
	<?php get_template_part('pagination'); ?>
</nav>
<?php get_footer(); ?>