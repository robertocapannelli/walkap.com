<?php get_header(); ?>

<?php $counter = 0; if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
	<?php $counter = $counter + 1 ; ?>
	<article id="post-<?php the_ID(); ?>" class="item-<?php echo $counter; ?>">
		<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title() ?></a></h2>
		<?php the_content() ?>
	</article>
	<?php endwhile; ?>
<?php else : ?>
	<article>
		<h2>Not Found</h2>
		<p>Sorry, but you are looking for something that isn't here</p>		
	</article>
<?php endif; $counter = 0; ?>

<?php get_sidebar(); ?>

<?php get_footer(); ?>