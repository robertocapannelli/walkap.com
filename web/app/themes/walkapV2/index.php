<?php if (!have_posts()) : ?>

	<div class="wrapper">
		
		<?php //get_template_part('templates/page', 'header'); ?>

		<div class="alert alert-warning">
	  
			<?php _e('Sorry, no results were found.', 'roots'); ?>
    
		</div>
		
		<p>Puoi provare ad effettuare di nuovo la ricerca tramite la form apposita altrimenti: </p>

		<ul>
			<li>Naviga tra le categorie cliccando il pulsante walkap </li>
			<li>Visita la <a href="<?php echo home_url('/') ?>">home</a></li>
			<li>Visita il mio <a href="<?php $portfolio = get_category_link(10); echo $portfolio;  ?>">portfolio</a></li>
		</ul>
  
		<?php //get_search_form(); ?>
	
	</div>

<?php endif; ?>


<div id="ms-container">
	
	<?php while (have_posts()) : the_post(); ?>
	
		<?php get_template_part('templates/content', get_post_format()); ?>
		
	<?php endwhile; ?>
	
</div>