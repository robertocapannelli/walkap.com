	<p class="bg-warning"><strong>Accedi</strong> o <strong>registrati</strong> per visualizzare tutti i contenuti di questo sito.</p>

	<div id="login1" class="login-box">
	
		<?php 
			
			$register = $_GET['register']; $reset = $_GET['reset']; 
			
			if ($register == true) { 
	
				echo '<h4 class="bg-success">Ora sei registrato.</h4>';
	                
				echo '<p>Controlla la tua e-mail ed accedi.</p>';
	
		 	} elseif ($reset == true) {
	
		 		echo '<h4 class="bg-info">Password Azzerata!</h4>';
	                
		 		echo '<p>Controlla la tua e-mail per cambiare la tua password ed accedi.</p>';
	
			} else { 
				
				echo '<h4>Sei già registrato?</h4>';
	                
				echo '<p>Accedi!</p>';
			
			} 
		 
		?>
	
	    <form method="post" action="<?php bloginfo('url') ?>/wp-login.php" class="wp-user-form" autocomplete="off">
		    
	        <div class="username form-group">
		        
	            <label for="user_login">Nome utente</label>
	            
	            <input type="text" name="log" placeholder="Inserisci il tuo nome utente" value="<?php echo esc_attr(stripslashes($user_login)); ?>" id="user_login" class="form-control"/>
	            
	        </div>
	        
	        <div class="password form-group">
		        
	            <label for="user_pass">Password</label>
	            
	            <input type="password" placeholder="Insersci la tua password" name="pwd" value="" id="user_pass" class="form-control"/>
	            
	        </div>
	        
	        <div class="login_fields">
	            
	            <?php do_action('login_form'); ?>
	            
	            <input type="submit" name="user-submit" value="Login" class="btn btn-default"/>
	            
	            <div class="rememberme checkbox">
		            
	                <label for="rememberme">
	                
	                    <input type="checkbox" name="rememberme" value="forever" checked="checked" id="rememberme" />Ricordami
	                    
	                </label>
	                
	            </div>
	            
	            <input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
	            
	            <input type="hidden" name="user-cookie" value="1" />
	            
	        </div>
	        
	    </form>
	    
	</div>
			
	<div id="login2" class="login-box">
		
	    <h4>Registrati</h4>
	    
	    <p>Registrati per accedere!</p>
	    
	    <form method="post" action="<?php echo site_url('wp-login.php?action=register', 'login_post') ?>" class="wp-user-form" autocomplete="off">
		    
	        <div class="username form-group">
		        
	            <label for="user_login">Nome utente</label>
	            
	            <input type="text" name="user_login" placeholder="Inserisci il nome utente che desideri" value="<?php echo esc_attr(stripslashes($user_login)); ?>" id="user_login" class="form-control" />
	            
	        </div>
	        
	        <div class="password form-group">
		        
	            <label for="user_email">E-mail</label>
	            
	            <input type="text" name="user_email" placeholder="Inserisci la tua e-mail" value="<?php echo esc_attr(stripslashes($user_email)); ?>" id="user_email" class="form-control" />
	            
	        </div>
	        
	        <div class="login_fields">
		        
	            <?php do_action('register_form'); ?>
	            
	            <input type="submit" name="user-submit" value="Registrati" class="btn btn-default" />
	            
	            <?php $register = $_GET['register']; if($register == true) { echo '<p>Riceverai una mail con la password!</p>'; } ?>
	            
	            <input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>?register=true" />
	            
	            <input type="hidden" name="user-cookie" value="1" />
	            
	        </div>
	        
	        <div class="g-recaptcha" data-sitekey="6Ld7oAYTAAAAAPgicRIMxsnfB_W7c69LDNa1O1j6"></div>
	        
	    </form>
	    
	</div>
			
	<div id="login3" class="login-box">
		
		<h4>Password dimenticata?</h4>
	    
	    <form method="post" action="<?php echo site_url('wp-login.php?action=lostpassword', 'login_post') ?>" class="wp-user-form" autocomplete="off">
		    
	    	<div class="username form-group">
		    	
	        	<label for="user_login">Nome utente o e-mail</label>
	        	
	            <input type="text" name="user_login" placeholder="Inserisci la tua e-mail o il tuo nome utente" value="" id="user_login" class="form-control" />
	            
	        </div>
	        
	        <div class="login_fields">
		        
		        <?php do_action('login_form', 'resetpass'); ?>
		        
	            <input type="submit" name="user-submit" value="Cambia password" class="btn btn-default" />
	            
				<?php
					
					$reset = $_GET['reset']; 
					
					if($reset == true) {
						
						echo '<p>Sarà mandato un messaggio al tuo indirizzo di posta.</p>'; 
						
					} 
					
				?>
				
	            <input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>?reset=true" />
	            
	            <input type="hidden" name="user-cookie" value="1" />
	            
	        </div>
	        
	    </form>
	    
	</div>
	
	<small class="full mail-check">Controlla la tua e-mail dopo la registrazione o dopo il recupero password.</small>