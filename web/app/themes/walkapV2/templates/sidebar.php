<div class="sidebar-wrapper">
	
	<?php
	
	if(is_single() && in_category(10)){ 
		
		echo '<section class="project-info">';
		
		if(get_post_meta($post->ID, 'wpcf-link-del-progetto', true)){
			
			echo '<h4><span class="icon-link"></span> URL del progetto</h4>';
		
			echo '<a href="' . get_post_meta($post->ID, 'wpcf-link-del-progetto', true) . '">' . get_post_meta($post->ID, 'wpcf-link-del-progetto', true) . '</a>';
			
		}
		
		if(get_post_meta($post->ID, 'wpcf-agency', true)){
			
			echo '<h4><span class="icon-office"></span> Agenzia</h4>';
		
			echo get_post_meta($post->ID, 'wpcf-agency', true);
			
		}
		
		if(get_post_meta($post->ID, 'wpcf-designer', true)){
			
			echo '<h4><span class="icon-pencil2"></span> Designer</h4>';
		
			echo get_post_meta($post->ID, 'wpcf-designer', true);
			
		}
		
		if(get_post_meta($post->ID, 'wpcf-developer', true)){
			
			echo '<h4><span class="icon-html5"></span> Developer</h4>';
		
			echo get_post_meta($post->ID, 'wpcf-developer', true);
			
		}
		
		if(get_post_meta($post->ID, 'wpcf-photographer', true)){
			
			echo '<h4><span class="icon-camera"></span> Photographer</h4>';
		
			echo get_post_meta($post->ID, 'wpcf-photographer', true);
			
		}
		
		if(get_post_meta($post->ID, 'wpcf-contributi', true)){
			
			echo '<h4><span class="icon-users"></span> Contributi</h4>';
		
			echo get_post_meta($post->ID, 'wpcf-contributi', true);
			
		}
		
		echo '</section>';
		
	} ?>

	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- walkap responsive -->
	<ins class="adsbygoogle"
	     style="display:block"
	     data-ad-client="ca-pub-5271751050627846"
	     data-ad-slot="6454448276"
	     data-ad-format="auto"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
	
	<?php dynamic_sidebar('sidebar-primary'); ?>
		
	<section>
	
		<h3><span class="icon-search"></span> Ricerca</h3>
		
		<?php get_template_part('templates/searchform'); ?>
	
	</section>
	
	<section>
		
		<h3><span class="icon-price-tags"></span> Tags Popolari</h3>
		
		<?php wp_tag_cloud('smallest=8&largest=26&unit=px&number=30&orderby=count&order=RAND&taxonomy=post_tag'); ?>
		
	</section>	
	
	<section class="links">
		
		<h3><span class="icon-link"></span> Links</h3>
		
		<ul>
		
			<?php wp_list_bookmarks('orderby=name&order=ASC&limit=-1&title_li=&categorize=0&class=links&topic_count_text_callback=my_tag_text_callback'); ?>
			
		</ul>
		
	</section>
	
	<section>
	
		<h3><span class="icon-cloud-download"></span> <a href="<?php echo get_permalink(5999); ?>">Downloads</a></h3>
	
		<h3><span class="icon-download3"></span> <a href="<?php echo get_permalink(6011); ?>">Torrents</a></h3>
		
	</section>
	
	<?php if(!is_page(5721)){ ?>
	
	<a href="http://www.philip-wood.com" target="_blank">
	
		<img src="<?php bloginfo('template_url'); ?>/assets/img/philip-wood-ad.jpg" alt="Philip Wood" title="Philip Wood" width="250" height="250" />
	
	</a>
	
	<?php } ?>
	
</div>