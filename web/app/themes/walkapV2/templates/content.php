<article <?php post_class('col-sm-6 ms-item'); ?> itemscope itemtype="http://schema.org/Article">
	
	<?php if(has_post_thumbnail() or in_category(7) or in_category(5) or in_category(6) or in_category(9) ) { ?>

		<div class="featured">
		      	
			<figure>
			    	
				<a href="<?php echo get_permalink(); ?>" itemprop="url">
					
					<?php 
						
						if(has_post_thumbnail()){ 
				    	
				    		echo get_the_post_thumbnail($post->ID,'thumbnail', array('class' => 'attachment-thumbnail img-responsive', 'title' => trim(strip_tags( $post->post_title ))),'itemprop=image');
				    		
				    	}else{
					    	
					    	$image = '';
					    	
					    	$bloginfo = get_bloginfo('template_url');
					    	
					    	if(in_category(7)){
						    	
						    	$image = $bloginfo . '/assets/img/github.jpg';
						    	
						    }elseif(in_category(5)){
							    
							    $image = $bloginfo . '/assets/img/delicious.jpg';
							    
						    }elseif(in_category(6)){
							    
							    $image = $bloginfo . '/assets/img/foursquare.jpg';
							    
						    }elseif(in_category(9)){
							    
							    $image = get_first_image();
							    
						    }
						    
						    echo '<img src="' . $image .'" alt="' . get_the_title() . '" title="' . get_the_title() . '" class="img-responsive" itemprop="image">';
					    	
				    	}
				    
				    ?>
			 		
			    </a>
			 
			</figure>
			  		
		</div>
  		
  	<?php 
	  	
	  	}else{
	  	
	  		//do nothing
	  	
  		} 
  	
  	?>
  	
  	<div class="wrapper">
  		
  		<header>
  			
  			<h2 class="entry-title">
  			
  				<a href="<?php the_permalink(); ?>" <?php if(in_category(7) or in_category(5) or in_category(6) or in_category(9)){ echo 'target="_blank"';} ?>><span itemprop="name"><?php the_title(); ?></span></a>
  				
  			</h2>
  			
  			<?php get_template_part('templates/entry-meta'); ?>
  			
  			<div class="tags">
  			
  				<?php the_tags('<span class="icon-price-tags"></span> ', ' ', ''); ?>
  				
  			</div>
  		
  		</header>  
  		
  		<div class="entry-summary">
		  	
		  	<?php the_excerpt(); ?>
		
		</div>
		
		<?php	
				
			$categories = get_the_category();
			$icon = '';
			if($categories){
				foreach($categories as $category){
					switch($category->slug) {
						case '4dummies':
							$icon = 'books';
							break;
						case 'portfolio':
							$icon = 'images';
							break;
						case 'blog':
							$icon = 'quill';
							break;
						case 'instagram':
							$icon = 'instagram';
							break;
						case 'github':
							$icon = 'github';
							break;
						case 'foursquare':
							$icon = 'location2';
							break;
						case 'video':
							$icon = 'video-camera';
							break;
						case 'delicious':
							$icon = 'delicious';
							break;
						case 'inspiration':
							$icon = 'bulb';
							break;
					}
				}
								
				echo '<a href="' . get_category_link($category->term_id) . '" title="' . $category->name . '" class="category-icon">';
				
				echo '<span class="icon-' . $icon . '" title="' . $category->name . '"></span>';
				
				echo '</a>';
			}
		
		?>
		
	</div>
	
</article>