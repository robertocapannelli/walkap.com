<?php /* Template Name: Torrents Template */  
	if ( is_user_logged_in() ) : 
		get_template_part('templates/content', 'page'); 
	else :
		echo '<div class="wrapper">';
		echo '<h2 class="bg-danger">Accesso negato!</h2>';
		echo '<p>Per visualizzare il contenuto di questa pagina devi essere registrato! Accedi al sito oppure registrati.</p>';
		echo '<a href="' . get_permalink(6173) . '" class="btn btn-default">Accedi o Registrati</a>';
		echo '</div>';
	endif;
?>