<footer class="content-info" role="contentinfo">
  <div class="container">
    <?php dynamic_sidebar('sidebar-footer'); ?>
    <p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> - L'onestà andrà di moda!</p>
  </div>
</footer>

<?php wp_footer(); ?>