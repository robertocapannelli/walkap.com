<?php while (have_posts()) : the_post(); ?>

	<article <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
	  
		<header>
	    
			<h1 class="entry-title"><span itemprop="name"><?php the_title(); ?></span></h1>
      
			<?php get_template_part('templates/entry-meta'); ?>
      
			<div class="tags">
	      
				<?php the_tags('<span class="icon-price-tags"></span> ', ' ', ' '); ?>
      	
      		</div>
      
    	</header>
    
		<div class="entry-content" itemprop="articleBody">
	    
			<?php the_content(); ?>
      
    	</div>
    
		<footer>
	    
			<?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'roots'), 'after' => '</p></nav>')); ?>
      
    	</footer>
    
		<?php comments_template('/templates/comments.php'); ?>
    
	</article>
  
<?php endwhile; ?>