<time class="published updated" datetime="<?php echo get_the_time('c'); ?>" itemprop="datePublished"><span class="icon-calendar"></span> <?php echo get_the_date(); ?></time>

<p class="byline author vcard"><?php echo __('By', 'roots'); ?>
	
	<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author" class="fn"><span itemprop="author" itemscope itemtype="http://schema.org/Person"><span itemprop="name"><?php echo get_the_author(); ?></span></span></a>
	
</p>
