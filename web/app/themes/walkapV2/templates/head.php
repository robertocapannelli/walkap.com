<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php wp_title('|', true, 'right'); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/assets/img/favicon.ico">
  <link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/assets/img/apple-touch-icon.png">
  <link href="https://plus.google.com/u/0/+RobertoCapannelli?rel=author">

  <?php wp_head(); ?>

  <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo esc_url(get_feed_link()); ?>">
  
  <script src='https://www.google.com/recaptcha/api.js'></script>
  
</head>
