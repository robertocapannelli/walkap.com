<?php /* Template Name: Registration */ ?>

<div class="wrapper">

	<?php 
		if ( is_user_logged_in() ) :
		
			global $current_user;
			
			echo '<h3>Ciao, ';
			
			if($current_user->user_firstname){
		    	
		    	echo $current_user->user_firstname . ' ' . $current_user->user_lastname . '</h3>';
		    	
		    }else{
			    
			    echo $current_user->user_login;
			    
			}
						
			echo '<h2>Hai effettuato correttamente l\'accesso!</h2>';
			
			echo '<p>Puoi aggiornare e modificare il tuo profilo in qualsiasi momento cliccando il pulsante <strong>"Entra nel Profilo"</strong> oppure puoi uscire dal profilo cliccando <strong>"Effettua il logout"</strong></p>';
			
			echo '<p><a href="' . get_edit_user_link() . '" class="btn btn-primary">Entra nel profilo</a></p>';
			
			echo '<p><a href="' . wp_logout_url(get_permalink()) . '" class="btn btn-danger">Effettua il logout</a></p>';
			
			echo '<p>Uscendo da questa pagina potrai sempre accedere o uscire dal tuo profilo attraverso la barra laterale alla tua sinistra, per vedere la barra laterale clicca su walkap in alto a sinistra.</p>';
			
		else:
		
			get_template_part('templates/registration-form');
			
			
		endif;			
	?>
	
</div>