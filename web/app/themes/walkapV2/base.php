<?php get_template_part('templates/head'); ?>

<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
	
<div id="sb-site">
	
	<!--[if lt IE 8]>
	  
    	<div class="alert alert-warning">
    
			<?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
      
		</div>
    
	<![endif]-->

	<?php
    	do_action('get_header');
    	// Use Bootstrap's navbar if enabled in config.php
    	if (current_theme_supports('bootstrap-top-navbar')) {
    	  get_template_part('templates/header-top-navbar');
    	} else {
    	  get_template_part('templates/header');
    	}
  	?>
    

  	<div class="wrap container" role="document">
	  
    	<div class="content row">
	    
			<main class="main <?php echo roots_main_class(); ?>" role="main">
	      
				<?php include roots_template_path(); ?>
        
				<?php if ($wp_query->max_num_pages > 1) : ?>
      	
				<nav class="post-nav">
		  		
					<ul class="pager">
					
					  <li class="previous"><?php next_posts_link(__('&larr; Older posts', 'roots')); ?></li>
					
					  <li class="next"><?php previous_posts_link(__('Newer posts &rarr;', 'roots')); ?></li>
					
					</ul>
			  	
				</nav>
			
				<?php endif; ?>
		
			</main><!-- /.main -->
      
			<?php if (roots_display_sidebar()) : ?>
			
			<aside class="sidebar <?php echo roots_sidebar_class(); ?>" role="complementary">
			
			  <?php include roots_sidebar_path(); ?>
			
			</aside><!-- /.sidebar -->
			
			<?php endif; ?>
      
    	</div><!-- /.content -->
    
	</div><!-- /.wrap -->

	<?php get_template_part('templates/footer'); ?>

</div>

<div class="sb-slidebar sb-left">
	<?php dynamic_sidebar('slidebar'); ?>
	<?php account_link(); ?>
	<section class="social-list">
		<ul>
			<li>
				<a href="https://www.facebook.com/roberto.capannelli" target="_blank" title="Facebook">
					<span class="icon-facebook2"></span>
				</a>
			</li>
			<li>
				<a href="https://www.youtube.com/user/robertocapannelli" target="_blank" title="YouTube">
					<span class="icon-youtube3"></span>
				</a>
			</li>
			<li>
				<a href="https://twitter.com/Rcapannelli" target="_blank" title="Twitter">
					<span class="icon-twitter"></span>
				</a>
			</li>
			<li>
				<a href="https://www.linkedin.com/in/robertocapannelli" target="_blank" title="LinkedIn">
					<span class="icon-linkedin2"></span>
				</a>
			</li>
			<li>
				<a href="https://instagram.com/robertocapannelli/" target="_blank" title="Instagram">
					<span class="icon-instagram"></span>
				</a>
			</li>
			<li>
				<a href="https://plus.google.com/+RobertoCapannelli/posts" target="_blank" title="Google+">
					<span class="icon-google-plus3"></span>
				</a>
			</li>
			<li>
				<a href="https://delicious.com/robbiecapannelli" target="_blank" title="Delicious">
					<span class="icon-delicious"></span>
				</a>
			</li>
		</ul>
	</section>
</div>

 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23645120-1', 'walkap.com');
  ga('require', 'displayfeatures');
  ga('require', 'linkid', 'linkid.js');
  ga('send', 'pageview');

</script>

</body>
</html>