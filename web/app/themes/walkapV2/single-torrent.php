<?php while (have_posts()) : the_post(); ?>

	<article <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
		
		<div class="featured">
			
			<figure>
		
		<?php if(has_post_thumbnail()){ echo get_the_post_thumbnail($post_id,'thumbnail','itemprop=image'); } ?>
		
			</figure>
		
		</div>
	  
		<header>
	    
			<h1 class="entry-title"><span itemprop="name"><?php the_title(); ?></span></h1>
      
    	</header>
    
		<div class="entry-content" itemprop="articleBody">
	    
			<?php the_content(); ?>
      
    	</div>
    
		<footer>
	    
			<?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'roots'), 'after' => '</p></nav>')); ?>
      
    	</footer>
    
		<?php comments_template('/templates/comments.php'); ?>
    
	</article>
  
<?php endwhile; ?>