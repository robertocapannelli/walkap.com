<div class="wrapper">

	<?php get_template_part('templates/page', 'header'); ?>

	<div class="alert alert-warning">
	
		<?php _e('Sorry, but the page you were trying to view does not exist.', 'roots'); ?>
  
	</div>

	<p><?php _e('It looks like this was the result of either:', 'roots'); ?></p>

	<ul>
	
		<li><?php _e('a mistyped address', 'roots'); ?></li>
  
		<li><?php _e('an out-of-date link', 'roots'); ?></li>
  
	</ul>

	<p>Puoi provare ad effettuare di nuovo la ricerca tramite la form apposita altrimenti: </p>

	<ul>
		<li>Naviga tra le categorie cliccando il pulsante walkap in alto a sinistra.</li>
		<li>Visita la <a href="<?php echo home_url('/') ?>">home</a></li>
		<li>Visita il mio <a href="<?php $portfolio = get_category_link(10); echo $portfolio;  ?>">portfolio</a></li>
	</ul>

</div>

<?php //get_search_form(); ?>
