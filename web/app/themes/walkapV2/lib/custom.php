<?php
/**
 * Custom functions
 */

/*===== Get the first image of the post =====*/

function get_first_image() {
	global $post, $posts;
	$first_img = '';
	ob_start();
	ob_end_clean();
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
	$first_img = $matches [1] [0];
	if(empty($first_img)){ //Defines a default image - enter manually the image path
    	$first_img = (get_bloginfo('template_url') . "/assets/img/segnaposto.gif");
  	}
  	return $first_img;
}

/*===== Get avatar from wordpress.com =====*/
function gravatar_function( $atts ){
	$id_or_email = get_the_author_meta('user_email');
	echo get_avatar( $id_or_email, $size = '96', $default = 'http://www.gravatar.com/avatar/4907e0ba9d9b20788ea6683cb09f0e17.png' );
}
add_shortcode( 'gravatar', 'gravatar_function' );


/*===== Hide the admin bar =====*/
show_admin_bar( false );

/*===== Function for the loop to display torrent and using shortcode in the editor =====*/

function torrents_loop(){
	$args = array('post_type' => 'torrent', 'showposts' => -1, 'order' => 'ASC');
	$torrents = new WP_Query($args);
	if ($torrents->have_posts()) : while ($torrents->have_posts()) : $torrents->the_post(); ?>
		<article <?php post_class('col-sm-6 ms-item'); ?> itemscope itemtype="http://schema.org/Article">
			<div class="featured">
			      	
				<figure>
				    	
					<a href="<?php echo get_permalink(); ?>" itemprop="url">
				    		
				    	<?php if(has_post_thumbnail()){ echo get_the_post_thumbnail($post_id,'thumbnail','itemprop=image'); } ?>		 		
				    </a>
				 
				</figure>
				  		
			</div>
			<div class="wrapper">
				
				<header>
					
					<h2 class="entry-title">
					
						<a href="<?php the_permalink(); ?>"><span itemprop="name"><?php the_title(); ?></span></a>
						
					</h2>
										
					<div class="tags">
					
						<?php the_tags('<span class="icon-price-tags"></span> ', ', ', ' '); ?>
						
					</div>
				
				</header>  
				
			</div>
		</article>
	<?php endwhile;else: ?>
		<p>Ci scusiamo ma state cercando qualcosa che non esiste!</p>
	<?php endif;
	wp_reset_postdata();
	wp_reset_query();
}

add_shortcode('torrent','torrents_loop');
	
/*===== Function to display Sign In and Sign Out in the sliderbar =====*/

function account_link(){
	echo '<div class="account-link">';
	if (is_user_logged_in()) { 
		global $current_user; 
		get_currentuserinfo();
	    echo '<span>Ciao, ';
	    	if($current_user->user_firstname){
		    	echo $current_user->user_firstname . ' ' . $current_user->user_lastname;
		    }else{
			    echo $current_user->user_login;
			}
		echo '</span>';
		echo '<a href="' . get_edit_user_link() . '" class="btn btn-primary btn-sm btn-block">Account</a> <a href="' . wp_logout_url(home_url()) . '" title="Logout" class="btn btn-default btn-sm btn-block">Esci</a>';
		} else {
			echo '<a href="' . get_permalink(6173) . '" class="btn btn-primary btn-sm btn-block">SignUp/SignIn</a>'; 
		}
	echo '</div>';
}

/*==== Shortcode for Accodion panel =====*/
function accordion_func($atts){
	$atts = shortcode_atts(array('problem' => 'No problem', 'solution' => ' '), $atts, 'accordion');
	return '<p>' . $atts['problem']  . '</p><div><script src="' . $atts['solution'] . '"></script></div>'; 
}
add_shortcode( 'accordion', 'accordion_func' );